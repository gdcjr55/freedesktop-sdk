From 337fce6ec1dfe3e4877ddc4b42fecd0d8ed7d7fb Mon Sep 17 00:00:00 2001
From: Manuel Virgilio <real_virgil@yahoo.it>
Date: Fri, 24 Jul 2020 22:32:57 +0200
Subject: [PATCH 2/2] fix endianness read/write

---
 webrtc/common_audio/wav_file.cc | 22 +++++++++++++++++-----
 1 file changed, 17 insertions(+), 5 deletions(-)

diff --git a/webrtc/common_audio/wav_file.cc b/webrtc/common_audio/wav_file.cc
index b14b620..835c0f4 100644
--- a/webrtc/common_audio/wav_file.cc
+++ b/webrtc/common_audio/wav_file.cc
@@ -13,6 +13,7 @@
 #include <algorithm>
 #include <cstdio>
 #include <limits>
+#include <byteswap.h>
 
 #include "webrtc/base/checks.h"
 #include "webrtc/base/safe_conversions.h"
@@ -64,14 +65,18 @@ WavReader::~WavReader() {
 }
 
 size_t WavReader::ReadSamples(size_t num_samples, int16_t* samples) {
-#ifndef WEBRTC_ARCH_LITTLE_ENDIAN
-#error "Need to convert samples to big-endian when reading from WAV file"
-#endif
   // There could be metadata after the audio; ensure we don't read it.
   num_samples = std::min(rtc::checked_cast<uint32_t>(num_samples),
                          num_samples_remaining_);
   const size_t read =
       fread(samples, sizeof(*samples), num_samples, file_handle_);
+
+#ifndef WEBRTC_ARCH_LITTLE_ENDIAN
+  for ( size_t idx = 0 ; idx < num_samples ; idx ++ ) {
+    samples[idx] = bswap_16(samples[idx]);
+  }
+#endif
+
   // If we didn't read what was requested, ensure we've reached the EOF.
   RTC_CHECK(read == num_samples || feof(file_handle_));
   RTC_CHECK_LE(read, num_samples_remaining_);
@@ -119,11 +124,18 @@ WavWriter::~WavWriter() {
 }
 
 void WavWriter::WriteSamples(const int16_t* samples, size_t num_samples) {
+  static const size_t kChunksize = 4096 / sizeof(uint16_t);
 #ifndef WEBRTC_ARCH_LITTLE_ENDIAN
-#error "Need to convert samples to little-endian when writing to WAV file"
-#endif
+  int16_t isamples[kChunksize];
+  for ( size_t idx = 0 ; idx < num_samples ; idx ++ ) {
+    isamples[idx] = bswap_16(samples[idx]);
+  }
+  const size_t written =
+    fwrite(samples, sizeof(*isamples), num_samples, file_handle_);
+#else
   const size_t written =
       fwrite(samples, sizeof(*samples), num_samples, file_handle_);
+#endif
   RTC_CHECK_EQ(num_samples, written);
   num_samples_ += static_cast<uint32_t>(written);
   RTC_CHECK(written <= std::numeric_limits<uint32_t>::max() ||
-- 
2.17.1

